//! Handles Kubernetes resources based Helm charts
//!
//! # Configuration
//!
//! Configuration happens via environment variables. The following variables *have to be* set:
//! - `GITLAB_DEPLOY_TOKEN`: Token which is sent by GitLab
//! - `GITLAB_REGISTRY_USER`: User used for accessing Chart registry
//! - `GITLAB_REGISTRY_TOKEN`: Token used for accessing Chart registry
//! - `GITLAB_REGISTRY: OCI-compliant URL to the Gitlab Helm registry
//!
//! This variables *can* be set:
//! - `RUST_LOG`: Log level, defaults to _warn_
//! - `HOST`: `<hostname>:<port> on which the service runs. Defaults to _0.0.0.0:3000_
//!

use crate::helm::ChartReleases;
use anyhow::{Context, Result};
use axum::{response::Redirect, routing, Router};
use helm::Charts;
use log::{debug, info};
use std::{
    env,
    sync::{Arc, Mutex},
};
use tower_http::trace::{DefaultMakeSpan, DefaultOnResponse, TraceLayer};
use tracing::Level;

#[derive(Clone)]
pub struct AppState {
    pub gitlab_secure_token: Arc<String>,
    pub releases: Arc<Mutex<ChartReleases>>,
    pub tracked_charts: Arc<Mutex<Option<Charts>>>,
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .with_target(false)
        .with_line_number(false)
        .with_file(false)
        .without_time()
        .init();

    let host_name = env::var("HOST").unwrap_or("0.0.0.0:3000".to_string());
    info!("Fetching all chart releases");

    info!("Setting up server");
    let gitlab_secure_token =
        Arc::new(env::var("GITLAB_DEPLOY_TOKEN").expect("Gitlab deploy token not present"));
    let gitlab_registry_user =
        Arc::new(env::var("GITLAB_REGISTRY_USER").expect("Gitlab registry user not present"));
    let gitlab_registry_token =
        Arc::new(env::var("GITLAB_REGISTRY_TOKEN").expect("Gitlab registry token not present"));
    helm::helm_registry_login(&gitlab_registry_user, &gitlab_registry_token)
        .context("Log in to Chart registry")?;

    let releases = helm::helm_list()?;
    debug!("{} charts found: {}", releases.size(), releases.as_list());
    let releases = Arc::new(Mutex::new(releases));
    let app_state = AppState {
        gitlab_secure_token,
        releases,
        tracked_charts: Arc::new(Mutex::new(None)),
    };

    let app = Router::new()
        .route(
            "/",
            routing::get(|| async { Redirect::temporary("/api/health") }),
        )
        .route("/api/update", routing::post(handlers::update))
        .route("/api/health", routing::get(handlers::health))
        .route("/api/status/:id", routing::get(handlers::status))
        .with_state(app_state.into())
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(DefaultMakeSpan::new().level(Level::INFO))
                .on_response(DefaultOnResponse::new().level(Level::INFO)),
        );
    //.route_layer(ServiceBuilder::new().layer(TraceLayer::new_for_http()));

    info!("Starting server on {}", &host_name);
    axum::Server::bind(&host_name.parse().context("Host address parsing failed")?)
        .serve(app.into_make_service())
        .await
        .context("Server initialisation failed")?;

    helm::helm_registry_logout().context("Logging out from Chart registry")?;

    Ok(())
}

/// Contains handlers
mod handlers {
    use std::sync::Arc;

    use crate::consolidation::ChartDiff;
    use crate::helm::Charts;
    use crate::AppState;
    use axum::extract::Path;
    use axum::http::HeaderMap;
    use axum::response::Json;
    use axum::{extract::State, http::StatusCode};
    use log::{debug, error, warn};
    use serde::Serialize;
    use serde_json::{json, Value};

    use super::helm;

    const APP_VERSION: Option<&str> = option_env!("CARGO_PKG_VERSION");
    const APP_NAME: Option<&str> = option_env!("CARGO_PKG_NAME");

    #[derive(Serialize, Default)]
    struct UpdateResponse {
        created: Vec<String>,
        deleted: Vec<String>,
        updated: Vec<String>,
        unchanged: Vec<String>,
        errors: Vec<String>,
    }

    impl UpdateResponse {
        pub fn add_error(&mut self, error: &anyhow::Error) {
            error!("Error detected: {}", error);
            self.errors.push(error.to_string())
        }

        pub fn add_success(&mut self, chart_diff: &ChartDiff) {
            match chart_diff {
                ChartDiff::Create(chart) => self
                    .created
                    .push(format!("{}:{}", chart.name, chart.version)),
                ChartDiff::Update(chart) => self
                    .updated
                    .push(format!("{}:{}", chart.name, chart.version)),
                ChartDiff::Delete(chart) => self
                    .deleted
                    .push(format!("{}:{}", chart.name, chart.version)),
                ChartDiff::Same(chart) => self
                    .unchanged
                    .push(format!("{}:{}", chart.name, chart.version)),
            }
        }

        pub fn build_response(self) -> (StatusCode, Json<Value>) {
            if self.errors.is_empty() {
                let response_body = json!(self);
                debug!("Response body:\n{}", &response_body);
                (StatusCode::OK, Json(response_body))
            } else {
                let response_body = json!(self);
                debug!("Response body:\n{}", &response_body);
                (StatusCode::INTERNAL_SERVER_ERROR, Json(response_body))
            }
        }
    }

    pub async fn health() -> Json<Value> {
        Json(json!({
            "name": APP_NAME.unwrap_or("<unknown>"),
            "version": APP_VERSION.unwrap_or("<unknown>"),
            "status": "running"
        }))
    }

    pub async fn status(
        Path(helm_chart_name): Path<String>,
        State(state): State<Arc<AppState>>,
    ) -> (StatusCode, String) {
        match state
            .releases
            .lock()
            .expect("Mutex on releases list poisoned!")
            .get_version(&helm_chart_name)
        {
            Some(v) => (StatusCode::OK, v),
            None => (
                StatusCode::NOT_FOUND,
                format!("{} does not exist", helm_chart_name),
            ),
        }
    }

    pub async fn update(
        State(state): State<Arc<AppState>>,
        headers: HeaderMap,
        Json(payload): Json<Value>,
    ) -> (StatusCode, Json<Value>) {
        if !check_secure_token(headers, &state.gitlab_secure_token) {
            return (
                StatusCode::FORBIDDEN,
                Json(serde_json::from_str(r#"{"errors": ["Access denied"]}"#).unwrap()),
            );
        };
        let mut response = UpdateResponse::default();

        let charts = match Charts::from_request(payload) {
            Ok(c) => c,
            Err(ref e) => {
                response.add_error(e);
                return response.build_response();
            }
        };
        let helm_list = match helm::helm_list() {
            Ok(hl) => hl,
            Err(ref e) => {
                response.add_error(e);
                return response.build_response();
            }
        };
        let released_charts = match Charts::from_helm_list(&helm_list) {
            Ok(c) => c,
            Err(ref e) => {
                response.add_error(e);
                return response.build_response();
            }
        };
        let mut tracked_charts = state.tracked_charts.lock().expect("Poisoned mutex!");
        let mut releases = state.releases.lock().expect("Poisoned mutext!");
        let diffs = charts.delta(&tracked_charts, &released_charts);
        let mut new_charts = Charts::default();
        for diff in diffs.chart_diffs.iter() {
            let res = match diff {
                ChartDiff::Create(ref chart) => helm::helm_upgrade(chart),
                ChartDiff::Update(ref chart) => helm::helm_upgrade(chart),
                ChartDiff::Delete(ref chart) => helm::helm_uninstall(chart),
                ChartDiff::Same(ref chart) => Ok(chart),
            };
            match res {
                Ok(chart) => {
                    response.add_success(diff);
                    new_charts.add_chart(chart)
                }
                Err(ref e) => response.add_error(e),
            }
        }
        *releases = helm_list;
        *tracked_charts = Some(new_charts);
        debug!("{} releases modified", diffs.chart_diffs.len());
        response.build_response()
    }

    fn check_secure_token(headers: HeaderMap, secure_token: &str) -> bool {
        if let Some(header_value) = headers.get("X-Gitlab-Token") {
            if let Ok(sent_token) = header_value.to_str() {
                if sent_token == secure_token {
                    debug!("Valid Gitlab secure token");
                    true
                } else {
                    warn!("Gitlab secure token not valid!");
                    false
                }
            } else {
                warn!("Gitlab secure token contains non-ASCII characters");
                false
            }
        } else {
            warn!("Request does not contain Gitlab secure token header");
            false
        }
    }
}

mod helm {
    use anyhow::{bail, Result};
    use log::{debug, error};
    use serde::{Deserialize, Serialize};
    use serde_json::Value;
    use std::{env, process::Command};

    #[derive(Clone)]
    pub struct Chart {
        pub name: String,
        pub version: String,
    }

    #[derive(Default)]
    pub struct Charts {
        pub(crate) charts: Vec<Chart>,
    }

    #[derive(Deserialize)]
    struct RegistryChart {
        pub name: String,
        pub version: String,
    }

    impl Charts {
        pub(crate) fn add_chart(&mut self, chart: &Chart) {
            self.charts.push(chart.clone())
        }

        pub fn from_request(payload: Value) -> Result<Self> {
            let mut charts_obj = Self { charts: vec![] };
            if let Some(arr) = payload.as_array() {
                let charts: Vec<RegistryChart> = arr
                    .iter()
                    .map(|e| serde_json::from_value(e.clone()))
                    .collect::<serde_json::Result<Vec<RegistryChart>>>()?;
                for chart in charts {
                    let new_chart = Chart {
                        name: chart.name,
                        version: chart.version,
                    };
                    charts_obj.add_chart(&new_chart);
                }
            }
            Ok(charts_obj)
        }

        pub fn from_helm_list(helm_list: &ChartReleases) -> Result<Self> {
            let mut charts_obj = Self { charts: vec![] };
            for chart in helm_list.0.iter() {
                let name = chart.name.clone();
                let version = if let Some(v) = chart.chart.rsplit('-').next() {
                    v.to_string()
                } else {
                    bail!("Chart has no version!")
                };
                let new_chart = Chart { name, version };
                charts_obj.add_chart(&new_chart);
            }
            Ok(charts_obj)
        }
    }

    pub struct ChartReleases(Vec<ChartRelease>);

    impl ChartReleases {
        pub fn add_release(&mut self, helm_chart: ChartRelease) {
            self.0.push(helm_chart)
        }

        pub fn get_version(&self, name: &str) -> Option<String> {
            self.0
                .iter()
                .find(|hc| hc.name == name)
                .map(|hc| hc.app_version.clone())
        }

        pub fn size(&self) -> usize {
            self.0.len()
        }

        pub fn as_list(&self) -> String {
            self.0
                .iter()
                .map(|hc| hc.name.to_string())
                .collect::<Vec<String>>()
                .join(", ")
        }
    }

    #[derive(Serialize, Deserialize, Debug)]
    pub struct ChartRelease {
        pub name: String,
        pub namespace: String,
        pub revision: u16,
        pub updated: String,
        pub status: String,
        pub chart: String,
        pub app_version: String,
    }

    pub fn helm_registry_login(user: &str, token: &str) -> Result<()> {
        let output = Command::new("helm")
            .arg("registry")
            .arg("login")
            .arg("-u")
            .arg(user)
            .arg("-p")
            .arg(token)
            .arg("cr.gitlab.switch.ch")
            .output();
        match output {
            Ok(o) => {
                if o.status.success() {
                    debug!(
                        "{}",
                        String::from_utf8(o.stdout).unwrap_or("<unknown output>".to_string())
                    );
                    Ok(())
                } else {
                    let err_msg = String::from_utf8(o.stderr)?;
                    error!("{}", &err_msg);
                    bail!("{}", err_msg)
                }
            }
            Err(_) => {
                let err_msg = "Execution of command `helm registry login` failed!";
                error!("{}", &err_msg);
                bail!("{}", &err_msg)
            }
        }
    }

    pub fn helm_registry_logout() -> Result<()> {
        let output = Command::new("helm")
            .arg("registry")
            .arg("logout")
            .arg("cr.gitlab.switch.ch")
            .output();
        match output {
            Ok(o) => {
                if o.status.success() {
                    Ok(())
                } else {
                    let err_msg = String::from_utf8(o.stderr)?;
                    error!("{}", err_msg);
                    bail!("{}", err_msg)
                }
            }
            Err(_) => {
                let err_msg = "Execution of command `helm registry logout` failed!";
                error!("{}", err_msg);
                bail!("{}", err_msg)
            }
        }
    }

    pub fn helm_list() -> Result<ChartReleases> {
        let mut helm_charts = ChartReleases(vec![]);
        loop {
            if let Ok(output) = Command::new("helm")
                .arg("list")
                .arg("--max")
                .arg("1024")
                .arg("--offset")
                .arg(helm_charts.size().to_string())
                .output()
            {
                if let Ok(parsed_output) = String::from_utf8(output.stdout) {
                    let output_lines = parsed_output.split('\n').skip(1);
                    for line in output_lines {
                        let mut cols = line.split('\t');
                        helm_charts.add_release(ChartRelease {
                            name: cols
                                .next()
                                .map(|e| e.trim().to_owned())
                                .unwrap_or(String::new()),
                            namespace: cols
                                .next()
                                .map(|e| e.trim().to_owned())
                                .unwrap_or(String::new()),
                            revision: cols
                                .next()
                                .map(|e| e.parse::<u16>().unwrap_or(1))
                                .unwrap_or(1),
                            updated: cols
                                .next()
                                .map(|e| e.trim().to_owned())
                                .unwrap_or(String::new()),
                            status: cols
                                .next()
                                .map(|e| e.trim().to_owned())
                                .unwrap_or(String::new()),
                            chart: cols
                                .next()
                                .map(|e| e.trim().to_owned())
                                .unwrap_or(String::new()),
                            app_version: cols
                                .next()
                                .map(|e| e.trim().to_owned())
                                .unwrap_or(String::new()),
                        });
                    }
                    if helm_charts.size() % 256 != 0 {
                        break;
                    }
                } else {
                    bail!("Parsing of stdout of command helm list failed")
                }
            } else {
                bail!("Command helm list failed")
            }
        }
        Ok(helm_charts)
    }

    pub fn helm_upgrade(chart: &Chart) -> Result<&Chart> {
        let helm_repo = env::var("GITLAB_REGISTRY").unwrap_or(
            "oci://cr.gitlab.switch.ch/memoriav/memobase-2020/configurations/helm-charts-registry/"
                .to_string(),
        );
        let output = Command::new("helm")
            .arg("upgrade")
            .arg("--atomic")
            .arg("-i")
            .arg(&chart.name)
            .arg(format!("{}{}", helm_repo, chart.name))
            .arg("--history-max")
            .arg("1")
            .arg("--debug")
            .arg("--version")
            .arg(&chart.version)
            .output();
        match output {
            Ok(o) => {
                if o.status.success() {
                    debug!(
                        "{}",
                        String::from_utf8(o.stdout).unwrap_or("<unknown output>".to_string())
                    );
                    Ok(chart)
                } else {
                    let out_msg = String::from_utf8(o.stdout)?;
                    let err_msg = String::from_utf8(o.stderr)?;
                    debug!("{}", out_msg);
                    bail!("{}", err_msg)
                }
            }
            Err(_) => bail!("Execution of command `helm upgrade` failed!"),
        }
    }

    pub fn helm_uninstall(chart: &Chart) -> Result<&Chart> {
        let output = Command::new("helm")
            .arg("uninstall")
            .arg(&chart.name)
            .output();
        match output {
            Ok(o) => {
                if o.status.success() {
                    debug!(
                        "{}",
                        String::from_utf8(o.stdout).unwrap_or("<unknown output>".to_string())
                    );
                    Ok(chart)
                } else {
                    let err_msg = String::from_utf8(o.stderr)?;
                    bail!("{}", err_msg)
                }
            }
            Err(_) => bail!("Execution of command `helm uninstall` failed!"),
        }
    }
}

mod consolidation {
    use log::debug;

    use crate::helm::{Chart, Charts};

    pub enum ChartDiff {
        Create(Chart),
        Update(Chart),
        Delete(Chart),
        Same(Chart),
    }

    #[derive(Default)]
    pub struct ChartDiffs {
        pub chart_diffs: Vec<ChartDiff>,
    }

    impl Charts {
        fn find_deleted_charts(&self, old_charts: &Charts) -> ChartDiffs {
            let mut deleted_charts = ChartDiffs::default();
            'outer: for old_chart in old_charts.charts.iter() {
                for new_chart in self.charts.iter() {
                    if old_chart.name == new_chart.name {
                        continue 'outer;
                    }
                }
                debug!(
                    "Deleted chart (name: {}; version: {}) detected",
                    old_chart.name, old_chart.version,
                );
                deleted_charts.add_chart_diff(ChartDiff::Delete(old_chart.to_owned()));
            }
            deleted_charts
        }

        fn find_new_and_updated_charts(&self, helm_releases: &Charts) -> ChartDiffs {
            let mut new_and_updated_charts = ChartDiffs::default();
            'outer: for new_chart in self.charts.iter() {
                for helm_release in helm_releases.charts.iter() {
                    if new_chart.name == helm_release.name {
                        if new_chart.version != helm_release.version {
                            debug!(
                                "Updated chart (name: {}; version: {} -> {}) detected",
                                new_chart.name, helm_release.version, new_chart.version,
                            );
                            new_and_updated_charts
                                .add_chart_diff(ChartDiff::Update(new_chart.to_owned()));
                        } else {
                            debug!(
                                "Unchanged chart (name: {}; version: {}) detected",
                                new_chart.name, new_chart.version,
                            );
                            new_and_updated_charts
                                .add_chart_diff(ChartDiff::Same(new_chart.to_owned()));
                        }
                        continue 'outer;
                    }
                }
                debug!(
                    "Newly created chart (name: {}; version: {}) detected",
                    new_chart.name, new_chart.version,
                );
                new_and_updated_charts.add_chart_diff(ChartDiff::Create(new_chart.to_owned()))
            }
            new_and_updated_charts
        }

        pub fn delta(&self, old_charts: &Option<Charts>, current_releases: &Charts) -> ChartDiffs {
            let mut chart_diffs = ChartDiffs::default();
            chart_diffs.add_chart_diffs(self.find_new_and_updated_charts(current_releases));
            if let Some(c) = old_charts {
                chart_diffs.add_chart_diffs(self.find_deleted_charts(c));
            }
            chart_diffs
        }
    }

    impl ChartDiffs {
        pub fn add_chart_diff(&mut self, chart_diff: ChartDiff) {
            self.chart_diffs.push(chart_diff)
        }

        pub fn add_chart_diffs(&mut self, chart_diffs: ChartDiffs) {
            self.chart_diffs.extend(chart_diffs.chart_diffs)
        }
    }

    #[cfg(test)]
    mod tests {

        use super::*;
        use crate::Charts;

        impl ChartDiff {
            pub fn is_create(&self) -> bool {
                match self {
                    ChartDiff::Create(_) => true,
                    _ => false,
                }
            }
            pub fn is_delete(&self) -> bool {
                match self {
                    ChartDiff::Delete(_) => true,
                    _ => false,
                }
            }
            pub fn is_update(&self) -> bool {
                match self {
                    ChartDiff::Update(_) => true,
                    _ => false,
                }
            }
            pub fn is_same(&self) -> bool {
                match self {
                    ChartDiff::Same(_) => true,
                    _ => false,
                }
            }
        }

        #[test]
        fn create_chart() {
            let charts_from_repo = Charts {
                charts: vec![Chart {
                    name: "a_chart".to_string(),
                    version: "1.0.0".to_string(),
                }],
            };
            let releases = Charts { charts: vec![] };
            let new_and_updated_charts = charts_from_repo.find_new_and_updated_charts(&releases);
            assert!(new_and_updated_charts
                .chart_diffs
                .first()
                .unwrap()
                .is_create());
            let deleted_charts = charts_from_repo.find_deleted_charts(&releases);
            assert!(deleted_charts.chart_diffs.is_empty());
        }

        #[test]
        fn update_chart() {
            let charts_from_repo = Charts {
                charts: vec![Chart {
                    name: "a_chart".to_string(),
                    version: "1.0.1".to_string(),
                }],
            };
            let releases = Charts {
                charts: vec![Chart {
                    name: "a_chart".to_string(),
                    version: "1.0.0".to_string(),
                }],
            };
            let new_and_updated_charts = charts_from_repo.find_new_and_updated_charts(&releases);
            assert!(new_and_updated_charts
                .chart_diffs
                .first()
                .unwrap()
                .is_update());
            let deleted_charts = charts_from_repo.find_deleted_charts(&releases);
            assert!(deleted_charts.chart_diffs.is_empty());
        }

        #[test]
        fn same_chart() {
            let charts_from_repo = Charts {
                charts: vec![Chart {
                    name: "a_chart".to_string(),
                    version: "1.0.0".to_string(),
                }],
            };
            let releases = Charts {
                charts: vec![Chart {
                    name: "a_chart".to_string(),
                    version: "1.0.0".to_string(),
                }],
            };
            let new_and_updated_charts = charts_from_repo.find_new_and_updated_charts(&releases);
            assert!(new_and_updated_charts
                .chart_diffs
                .first()
                .unwrap()
                .is_same());
            let deleted_charts = charts_from_repo.find_deleted_charts(&releases);
            assert!(deleted_charts.chart_diffs.is_empty());
        }

        #[test]
        fn deleted_chart() {
            let charts_from_repo = Charts { charts: vec![] };
            let releases = Charts {
                charts: vec![Chart {
                    name: "a_chart".to_string(),
                    version: "1.0.0".to_string(),
                }],
            };
            let new_and_updated_charts = charts_from_repo.find_new_and_updated_charts(&releases);
            assert!(new_and_updated_charts.chart_diffs.is_empty());
            let deleted_charts = charts_from_repo.find_deleted_charts(&releases);
            assert!(deleted_charts.chart_diffs.first().unwrap().is_delete());
        }
    }
}
