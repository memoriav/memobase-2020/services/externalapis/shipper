# Shipper

## Strategy

1. git pull the latest changes from the remote repository
2. create a list of all available charts and their respective versions
3. create a list of all deployed charts
4. compare the remote list, the list of deployed charts and the former remote list. Considered are:
    - chart exists in remote list, but not in deployed charts list => CREATE
    - chart exists in remote list and in deployed charts list, but the versions differ => UPDATE
    - chart exists in remote list, but not in former remote list => DELETE
   Ignored are charts with existing releases, but without being in the remote list AND in the former remote list
5. redeploy charts according to the results of the above step
