FROM cr.gitlab.switch.ch/memoriav/memobase-2020/utilities/rust-with-upx:latest AS build
ARG APPNAME=shipper
ARG HELM_VERSION=3.11.1

WORKDIR /buildenv
RUN USER=root cargo new -q --vcs none --bin $APPNAME
WORKDIR /buildenv/$APPNAME
COPY . .
RUN cargo build --release
RUN mkdir /build && mv ./target/release/$APPNAME /build/app
RUN wget https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && tar xzf helm-v${HELM_VERSION}-linux-amd64.tar.gz && mv linux-amd64/helm /build/

FROM alpine
WORKDIR /app
COPY --from=build /build/app app
COPY --from=build /build/helm /usr/bin/
ENTRYPOINT ["./app"]
